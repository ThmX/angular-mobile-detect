/*
 * Copyright (c) 2016.
 * Thomas Denoréaz (thomas.denoreaz@gmail.com)
 */

var webpack = require('webpack');
var webpackConfig = require('./webpack.config');

module.exports = function (config) {
    config.set({
        singleRun: true,
        frameworks: ['jasmine'],
        browsers: [
            'PhantomJS',
            'Chrome'
        ],
        files: [
            'test/**/*.spec.js'
        ],
        preprocessors: {
            'test/**/*.spec.js': ['webpack']
        },
        reporters: ['dots', 'coverage'],
        coverageReporter: {
            type: 'text',
            dir: 'test-coverages'
        },
        webpack: webpackConfig,
        webpackServer: {
            noInfo: true
        }
    });
};
