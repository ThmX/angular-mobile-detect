/*
 * Copyright (c) 2016.
 * Thomas Denoréaz (thomas.denoreaz@gmail.com)
 */

import angular from 'angular';
import 'angular-mocks';

import '../src/AngularMobileDetect';

const UA = {
    desktop: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36',
    nexus: 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36'
};

describe('AngularMobileDetect', () => {

    beforeEach(angular.mock.module('angular-mobile-detect'));

    let $window;
    beforeEach(inject(_$window_ => {
        $window = _$window_;
    }));

    it('defines the md service', inject((md) => {
        expect(md).toBeDefined();
    }));
});