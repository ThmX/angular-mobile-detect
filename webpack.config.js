/*
 * Copyright (c) 2016.
 * Thomas Denoréaz (thomas.denoreaz@gmail.com)
 */

'use strict';

var webpack = require('webpack');
var path = require('path');

var webpackConfig = {
    devtool: 'source-map',
    progress: false,
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                exclude: [
                    path.resolve('src/'),
                    path.resolve('node_modules/')
                ],
                loader: 'babel'
            },
            {
                test: [
                    /\.js$/,
                    /!\.spec\.js$/
                ],
                include: path.resolve('src/'),
                loader: 'isparta'
            }
        ]
    },
    plugins: [],
    node: {
        fs: 'empty' // https://github.com/webpack/jade-loader/issues/8#issuecomment-55568520
    },
    isparta: {
        embedSource: true,
        noAutoWrap: true
    },
    stats: {
        colors: true,
        modules: false,
        reasons: true
    }
};

/*
if (process.env.NODE_ENV === 'production') {
    var uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    });
    webpackConfig.plugins.push(uglifyJsPlugin);
}
*/

module.exports = webpackConfig;
