/*
 * Copyright (c) 2016.
 * Thomas Denoréaz (thomas.denoreaz@gmail.com)
 */

import angular from 'angular';
import MobileDetect from 'mobile-detect';

angular.module('angular-mobile-detect', [])
    .factory('md', ['$window', ($window) => {
        return new MobileDetect($window.navigator.userAgent);
    }])
    .directive('mdMobile', () => {
        return {
            restrict: 'E',
            scope: {
                translation: '=ngModel',
                languages: '='
            },
            templateUrl: 'mb-translators',
            link(scope) {

            }
        };
    });