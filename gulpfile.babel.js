/*
 * Copyright (c) 2016.
 * Thomas Denoréaz (thomas.denoreaz@gmail.com)
 */

'use strict';

import gulp from 'gulp';
import eslint from 'gulp-eslint';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import named from 'vinyl-named';
import karma from 'karma';
import webpack from 'webpack-stream';
import webpackConfig from './webpack.config.js';

gulp.task('default', ['build', 'tests']);

gulp.task('build', ['build:lint', 'build:webpack', 'build:uglify']);
gulp.task('tests', ['tests:karma']);
gulp.task('deploy', ['deploy:npm']);

/*
 * Build
 */

gulp.task('build:lint', () => {
    return gulp.src(['src/**/*.js', '!node_modules/**'])
        .pipe(eslint('.eslintrc'))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('build:webpack', () => {
    return gulp.src('src/AngularMobileDetect.js')
        .pipe(named())
        .pipe(webpack(webpackConfig))
        .pipe(rename({
            basename: 'angular-mobile-detect'
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build:uglify', ['build:webpack'], () => {
    return gulp.src('dist/angular-mobile-detect.js')
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest('dist/'));
});

/*
 * Tests
 */

gulp.task('tests:karma', callback => {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true,
        browsers: ['PhantomJS']
    }, callback).start();
});

/*
 * Deploy
 */

gulp.task('deploy:npm', ['build'], () => {
    console.log("TBD");
});
